﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using LazerConsoleApp.Models;
using LazerConsoleApp.Properties;
using LazerConsoleApp.Services;

namespace LazerConsoleApp
{
    internal class Program
    {
        private static readonly string Dir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        public static void Main(string[] args)
        {
            Console.WriteLine(Resources.opening_message);
            Run();
            Console.ReadLine();
        }

        private static void Run()
        {
            var input = Console.ReadLine();
            if (!string.IsNullOrEmpty(input))
            {
                switch (input.ToLower())
                {
                    case "quit":
                    case "q":
                        Environment.Exit(0);
                        break;
                    case "test":
                    case "t":
                        Fire(ParseService.ParseFile(Resources.TestFile));
                        break;
                    default:
                        string filePath = $"{Dir}\\textfiles\\{input}";
                        if (!File.Exists(filePath))
                        {
                            Console.WriteLine(Resources.file_not_found, input);
                            Run();
                        }
                        else
                        {
                            Fire(ParseService.ParseFile(File.ReadAllText(filePath)));
                        }
                        break;
                }
            }
            else
            {
                Console.WriteLine(Resources.No_Input);
                Run();
            }
        }

        private static void Fire(IEnumerable<string> sections)
        {
            var house = House.GetDimensions(sections.ElementAt(0), sections.ElementAt(1));
            var laser = Laser.Create(sections.ElementAt(2));
            laser.Fire(house);
            Console.WriteLine(Resources.dimensions_of_board, house.Width, house.Height);
            Console.WriteLine(Resources.laser_start_position, laser.StartLocation);
            Console.WriteLine(Resources.laser_exited_house, laser.ExitLocation, laser.ExitOrientation);
        }
    }
}