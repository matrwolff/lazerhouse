﻿namespace LazerConsoleApp.Enums
{
    public enum Orientation
    {
        Horizontal,
        Vertical
    }
}