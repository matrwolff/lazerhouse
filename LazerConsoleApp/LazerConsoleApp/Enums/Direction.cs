﻿namespace LazerConsoleApp.Enums
{
    public enum Direction
    {
        Up,
        Right,
        Down,
        Left
    }
}