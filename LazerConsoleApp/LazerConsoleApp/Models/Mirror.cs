﻿using LazerConsoleApp.Enums;
using LazerConsoleApp.Services;

namespace LazerConsoleApp.Models
{
    public class Mirror
    {
        public Lean Lean;
        public bool LeftReflective;
        public bool RightReflective;

        public Direction Reflect(Direction direction)
        {
            switch (direction)
            {
                case Direction.Up:
                    if (Lean == Lean.Left && LeftReflective)
                        return Direction.Left;
                    if (Lean == Lean.Right && RightReflective)
                        return Direction.Right;
                    break;
                case Direction.Right:
                    if (Lean == Lean.Left && LeftReflective)
                        return Direction.Down;
                    if (Lean == Lean.Right && LeftReflective)
                        return Direction.Up;
                    break;
                case Direction.Down:
                    if (Lean == Lean.Right && LeftReflective)
                        return Direction.Left;
                    if (Lean == Lean.Left && RightReflective)
                        return Direction.Right;
                    break;
                case Direction.Left:
                    if (Lean == Lean.Left && RightReflective)
                        return Direction.Up;
                    if (Lean == Lean.Right && RightReflective)
                        return Direction.Down;
                    break;
            }
            return direction;
        }

        public static Mirror Create(string roomWithMirrorCoords)
        {
            var mirror = new Mirror();
            var mirrorProps = roomWithMirrorCoords.Trim();

            mirror.Lean = DirectionalService.ToSide(mirrorProps.Substring(0, 1));
            if (mirrorProps.Length == 1)
            {
                mirror.LeftReflective = true;
                mirror.RightReflective = true;
            }
            else
            {
                foreach (var prop in mirrorProps.Substring(1, mirrorProps.Length - 1).ToCharArray())
                    switch (DirectionalService.ToSide(prop.ToString()))
                    {
                        case Lean.Left:
                            mirror.LeftReflective = true;
                            break;
                        case Lean.Right:
                            mirror.RightReflective = true;
                            break;
                    }
            }
            return mirror;
        }
    }
}