﻿namespace LazerConsoleApp.Models
{
    public class Room
    {
        public Room()
        {
            Mirror = null;
        }

        public Mirror Mirror { get; set; }

        public bool HasMirror => Mirror != null;

        public static Room AddMirror(string roomWithMirror)
        {
            var room = new Room();
            if (!string.IsNullOrWhiteSpace(roomWithMirror))
                room.Mirror = Mirror.Create(roomWithMirror);
            return room;
        }
    }
}