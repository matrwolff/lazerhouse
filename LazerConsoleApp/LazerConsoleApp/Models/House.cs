﻿using System;
using System.Drawing;
using System.Linq;
using LazerConsoleApp.Services;

namespace LazerConsoleApp.Models
{
    public class House
    {
        public Room[,] Rooms;

        public House(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public int Width { get; set; }
        public int Height { get; set; }

        public static House GetDimensions(string houseDimensions, string roomsWithMirrors)
        {
            var dimensions = houseDimensions.Split(',');
            if (int.TryParse(dimensions.ElementAt(0), out var width) &&
                int.TryParse(dimensions.ElementAt(1), out var height))
            {
                var house = new House(width, height);
                house.CreateRooms();
                house.AddMirrorToRooms(roomsWithMirrors);
                return house;
            }
            throw new Exception("Invalid House Dimensions");
        }

        public bool RoomExists(Point point)
        {
            return point.X >= 0 &&
                   point.Y >= 0 &&
                   point.X < Width &&
                   point.Y < Height;
        }

        private void CreateRooms()
        {
            try
            {
                Rooms = new Room[Width, Height];

                for (var x = 0; x < Width; x++)
                for (var y = 0; y < Height; y++)
                    Rooms[x, y] = new Room();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new Exception("Could Not Create Rooms");
            }
        }

        private void AddMirrorToRooms(string roomsWithMirrors)
        {
            foreach (string room in ParseService.ParseRoomsWithMirrors(roomsWithMirrors))
            {
                var breakPoint = RoomService.FindCoordinateBreakPosition(room);
                var roomLocation = room.Substring(0, breakPoint).Trim().Split(',');
                if (int.TryParse(roomLocation.ElementAt(0), out int x) &&
                    int.TryParse(roomLocation.ElementAt(1), out int y))
                    Rooms[x, y] = Room.AddMirror(room.Substring(breakPoint, room.Length - breakPoint));
                else
                    throw new Exception("Invalid Room");
            }
        }
    }
}