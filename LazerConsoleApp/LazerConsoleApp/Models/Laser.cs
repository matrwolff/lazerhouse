﻿using System;
using System.Collections.Generic;
using System.Drawing;
using LazerConsoleApp.Enums;
using LazerConsoleApp.Extensions;
using LazerConsoleApp.Services;

namespace LazerConsoleApp.Models
{
    internal class Laser
    {
        private readonly Orientation _orientation;

        private readonly HashSet<int> _previousMoves = new HashSet<int>();
        private House _house;
        private Point _position;
        public Direction CurrentDirection;
        public Point ExitLocation;
        public Orientation ExitOrientation;
        public Direction StartDirection;
        public Point StartLocation;

        public Laser(Point position, Orientation orientation)
        {
            StartLocation = position;
            _orientation = orientation;
        }

        private Point _nextRoom
        {
            get
            {
                switch (CurrentDirection)
                {
                    case Direction.Up:
                        return _position.Up();
                    case Direction.Right:
                        return _position.Right();
                    case Direction.Down:
                        return _position.Down();
                    case Direction.Left:
                        return _position.Left();
                }
                throw new Exception("Could Not Determine Next Room");
            }
        }

        private bool CanMove => _house.RoomExists(_nextRoom);

        public static Laser Create(string laserProps)
        {
            return LaserServices.Create(laserProps.Trim());
        }

        public void Fire(House house)
        {
            _house = house;
            _position = StartLocation;
            FindStart();
            Move();
        }

        private void FindStart()
        {
            switch (_orientation)
            {
                case Orientation.Vertical:
                    if (!_house.RoomExists(_position.Up()))
                    {
                        StartDirection = Direction.Down;
                        break;
                    }
                    if (!_house.RoomExists(_position.Down()))
                        StartDirection = Direction.Up;
                    break;
                case Orientation.Horizontal:
                    if (!_house.RoomExists(_position.Left()))
                        StartDirection = Direction.Right;
                    else if (!_house.RoomExists(_position.Right()))
                        StartDirection = Direction.Left;
                    break;
            }
        }

        private void Move()
        {
            UpdateLaserPosition();
            if (CanMove)
            {
                var _currentPositionHash = (_position + CurrentDirection.ToString()).GetHashCode();
                if (_previousMoves.Contains(_currentPositionHash))
                    throw new Exception("Possible Infinite Loop Detected");
                _previousMoves.Add(_currentPositionHash);
                _position = _nextRoom;
                Move();
            }
            ExitLocation = _position;
            ExitOrientation = _orientation;
        }

        private void UpdateLaserPosition()
        {
            var room = _house.Rooms[_position.X, _position.Y];
            if (room.HasMirror)
                CurrentDirection = room.Mirror.Reflect(CurrentDirection);
        }
    }
}