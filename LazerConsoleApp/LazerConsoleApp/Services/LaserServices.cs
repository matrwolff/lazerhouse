﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using LazerConsoleApp.Enums;
using LazerConsoleApp.Models;

namespace LazerConsoleApp.Services
{
    internal class LaserServices
    {
        public static Laser Create(string laserPropsTrimmed)
        {
            var startingCoordinates = laserPropsTrimmed.Substring(0, laserPropsTrimmed.Length - 1);
            IEnumerable<string> coordinates = startingCoordinates.Split(',');
            if (int.TryParse(coordinates.ElementAt(0), out int x) && int.TryParse(coordinates.ElementAt(1), out int y))
                return new Laser(new Point(x, y),
                    GetOrientation(laserPropsTrimmed.Substring(laserPropsTrimmed.Length - 1, 1)));
            throw new Exception("Could not Create Laser");
        }

        private static Orientation GetOrientation(string orientation)
        {
            switch (orientation.Trim().ToUpper())
            {
                case "H":
                    return Orientation.Horizontal;
                case "V":
                    return Orientation.Vertical;
            }

            throw new Exception($"{orientation} is not a valid orientation.");
        }
    }
}