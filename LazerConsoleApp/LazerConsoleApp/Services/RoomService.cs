﻿namespace LazerConsoleApp.Services
{
    internal class RoomService
    {
        public static int FindCoordinateBreakPosition(string roomCoordinates)
        {
            for (var i = roomCoordinates.Length - 1; i > 0; i--)
                if (char.IsNumber(roomCoordinates[i]))
                    return i + 1;
            return 0;
        }
    }
}