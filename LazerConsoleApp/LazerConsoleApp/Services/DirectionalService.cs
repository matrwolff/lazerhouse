﻿using System;
using LazerConsoleApp.Enums;

namespace LazerConsoleApp.Services
{
    public class DirectionalService
    {
        public static Lean ToSide(string side)
        {
            switch (side.ToUpper())
            {
                case "L":
                    return Lean.Left;
                case "R":
                    return Lean.Right;
            }
            throw new Exception($"{side} is not a valid lean direction for a mirror");
        }
    }
}