﻿using System;
using System.Collections;

namespace LazerConsoleApp.Services
{
    internal class ParseService
    {
        public static string[] ParseFile(string fileContents)
        {
            try
            {
                return fileContents.ToUpper()
                    .Trim()
                    .Split(new[] {"-1"}, StringSplitOptions.RemoveEmptyEntries);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new Exception("Unable to Parse File");
            }
        }

        public static IEnumerable ParseRoomsWithMirrors(string roomsWithMirrors)
        {
            return roomsWithMirrors.Trim().Split(new[] {"\r\n", "\n"}, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}